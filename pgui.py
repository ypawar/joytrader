from PyQt4 import QtCore, QtGui
import sys

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class UiForm(object):
    def setupUi(self, Form):
        # type: (object) -> object
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(212, 167)
        self.verticalLayoutWidget = QtGui.QWidget(Form)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 194, 150))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.lineEditProfit = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.lineEditProfit.setObjectName(_fromUtf8("lineEditProfit"))
        self.gridLayout.addWidget(self.lineEditProfit, 2, 1, 1, 1)
        self.label_3 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
        self.lineEditQuantity = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.lineEditQuantity.setObjectName(_fromUtf8("lineEditQuantity"))
        self.gridLayout.addWidget(self.lineEditQuantity, 0, 1, 1, 1)
        self.label = QtGui.QLabel(self.verticalLayoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.lineEditStopLoss = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.lineEditStopLoss.setObjectName(_fromUtf8("lineEditStopLoss"))
        self.gridLayout.addWidget(self.lineEditStopLoss, 3, 1, 1, 1)
        self.label_4 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)
        self.lineEditOffset = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.lineEditOffset.setObjectName(_fromUtf8("lineEditOffset"))
        self.gridLayout.addWidget(self.lineEditOffset, 1, 1, 1, 1)
        self.label_2 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.pushButtonUpdate = QtGui.QPushButton(self.verticalLayoutWidget)
        self.pushButtonUpdate.setObjectName(_fromUtf8("pushButtonUpdate"))
        self.verticalLayout.addWidget(self.pushButtonUpdate)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "JOY TRADER", None))
        self.lineEditProfit.setText(_translate("Form", "5", None))
        self.label_3.setText(_translate("Form", "PROFIT", None))
        self.lineEditQuantity.setText(_translate("Form", "10", None))
        self.label.setText(_translate("Form", "QUANTITY", None))
        self.lineEditStopLoss.setText(_translate("Form", "5", None))
        self.label_4.setText(_translate("Form", "STOPLOSS", None))
        self.lineEditOffset.setText(_translate("Form", "2", None))
        self.label_2.setText(_translate("Form", "OFFSET", None))
        self.pushButtonUpdate.setText(_translate("Form", "UPDATE", None))

        self.intValidator = QtGui.QIntValidator(0, 9999)
        self.intValidator2 = QtGui.QIntValidator(0, 10)
        self.doubleValidator = QtGui.QDoubleValidator()

        self.lineEditOffset.setValidator(self.intValidator2)
        self.lineEditQuantity.setValidator(self.intValidator)
        self.lineEditProfit.setValidator(self.doubleValidator)
        self.lineEditStopLoss.setValidator(self.doubleValidator)

        self.instrument = ''
        self.orderType = ''
        self.orderVariety = ''
        self.product = ''
        self.quantity = 0
        self.quantity = int(self.lineEditQuantity.text())
        self.profit = float(self.lineEditProfit.text())
        self.stopLoss = float(self.lineEditStopLoss.text())
        self.offset = int(self.lineEditOffset.text())

        self.pushButtonUpdate.clicked.connect(self.updateValues)

    def updateValues(self):
        self.quantity = int(self.lineEditQuantity.text())
        self.profit = float(self.lineEditProfit.text())
        self.stopLoss = float(self.lineEditStopLoss.text())
        self.offset = int(self.lineEditOffset.text())
        print "Values Updated",'Quantity',self.quantity,'Profit',self.profit,'StopLoss',self.stopLoss,'Offset',self.offset

if __name__ == "__main__":
    # Setup GUI
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = UiForm()
    ui.setupUi(Form)
    Form.setFixedSize(212, 167)
    Form.show()
    sys.exit(app.exec_())
