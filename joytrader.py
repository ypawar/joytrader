import pgui

from os import path, remove
import logging

import ldc
import dbworker
import oexecutor
from kiteconnect import KiteConnect
from PyQt4 import QtCore, QtGui
import sys
import subprocess
import time

apikey= "k2zc7gdhcv1zuryi"
apisecret = "f06fswrycee3m013kgun9op2ybxbelse"
userid = "RC1434"
tknsym={}
symtkn={}
ifile='symtknlist.txt'
cmd = '''"C:\Program Files (x86)\NinjaTrader 7\\bin\NinjaTrader.exe"'''


def processExists(processname):
    tlcall = 'TASKLIST', '/FI', 'imagename eq %s' % processname
    # shell=True hides the shell window, stdout to PIPE enables
    # communicate() to get the tasklist command result
    tlproc = subprocess.Popen(tlcall, shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    # trimming it to the actual lines with information
    tlout = tlproc.communicate()[0].strip().split('\r\n')
    # if TASKLIST returns single line without processname: it's not running
    if len(tlout) > 1 and processname in tlout[-1]:
        # print('process "%s" is running!' % processname)
        return True
    else:
        # print(tlout[0])
        # print('process "%s" is NOT running!' % processname)
        return False


def makedicts():
    with open(ifile) as m:
        rows = m.readlines()
    for row in rows:
        sym = row.split(',')[0].strip()
        tkn = row.split(',')[1].strip()
        tknsym[int(tkn)] = sym
        symtkn[sym] = int(tkn)

if __name__ == '__main__':
    # Remove existing log file if present
    if path.isfile("joytrader.log"):
        remove("joytrader.log")

    # Create the Logger
    logger = logging.getLogger('joytrader')
    logger.propagate = False
    logger.setLevel(logging.INFO)

    # Create the Handler for logging data to a file
    logger_handler = logging.FileHandler('joytrader.log')
    logger_handler.setLevel(logging.DEBUG)

    # Create a Formatter for formatting the log messages
    logger_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # Add the Formatter to the Handler
    logger_handler.setFormatter(logger_formatter)

    # Add the Handler to the Logger
    logger.addHandler(logger_handler)
    logger.info('Completed configuring logger()!')
    logger.info('Creating Token Dictionaries')
    makedicts()
    logger.info('Created Token Dictionaries')

    logger.info('Setting up Kite API')
    # # Setup Kite API
    kite = KiteConnect(api_key=apikey)
    print kite.login_url()
    request_token = raw_input("Enter Request Token\n")
    data = kite.request_access_token(request_token, secret=apisecret)
    kite.set_access_token(data["access_token"])
    logger.info('Kite API set successfully')
    # Start DB
    db = dbworker.DB()

    # Start NT if not already
    while not processExists("NinjaTrader.exe"):
        # logger.info('Starting Ninjatrader')
        # print "Starting Ninja Trader"
        # subprocess.Popen(cmd)
        # time.sleep(15)
        print "Please start Ninja Trader and then press any key"
        raw_input()

    # Setup live data collector and feeder for NT
    liveDataCollector= ldc.LiveDataCollector(apisecret,data['public_token'],userid,db,tknsym)
    # Setup GUI
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = pgui.UiForm()
    ui.setupUi(Form)
    # Setup order executor
    ordrexecutor = oexecutor.OrderExecutor(ui,kite,symtkn,db)

    ordrexecutor.start()
    Form.show()
    exits = app.exec_()
    ordrexecutor.running=False
    liveDataCollector.close()
    db.logout()
    time.sleep(2)
    # Stop all other processes then exit
    sys.exit(exits)