import subprocess
import os
#
# processes = subprocess.call(['tasklist'],stdout=subprocess.PIPE)
# print processes

# path = '''"C:\Program Files (x86)\NinjaTrader 7\\bin\NinjaTrader.exe"'''
# print os.startfile(path)
import sys


def processExists(processname):
    tlcall = 'TASKLIST', '/FI', 'imagename eq %s' % processname
    # shell=True hides the shell window, stdout to PIPE enables
    # communicate() to get the tasklist command result
    tlproc = subprocess.Popen(tlcall, shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    # trimming it to the actual lines with information
    tlout = tlproc.communicate()[0].strip().split('\r\n')
    # if TASKLIST returns single line without processname: it's not running
    if len(tlout) > 1 and processname in tlout[-1]:
        # print('process "%s" is running!' % processname)
        return True
    else:
        # print(tlout[0])
        # print('process "%s" is NOT running!' % processname)
        return False
#
processExists('NinjaTrader.exe')
