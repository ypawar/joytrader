import zmq
import threading


class OrderExecutor (threading.Thread):
    def __init__(self,gui,kiteapi,symtkndict,dbctr):
        threading.Thread.__init__(self)
        self.running=True
        # Reference to Python GUI object to get dynamic values
        self.gui = gui
        # Reference to kita api object to place orders
        self.kite = kiteapi
        # Symbol to Token Dictionary
        self.symtkn = symtkndict
        # Database Connector
        self.db = dbctr

    def run(self):
        print "Starting Order Executor Server"
        # ZeroMQ Context
        self.context = zmq.Context()
        self.sock = self.context.socket(zmq.REP)
        self.sock.bind("tcp://127.0.0.1:5678")
        while self.running:
            try:
                # Receive Message
                message = self.sock.recv(flags=zmq.NOBLOCK)
                self.sock.send("OK")
                action,symbol=message.split(',')
                price = self.db.getLTP(self.symtkn[symbol])
                self.placeorder(action,symbol,price)

            except zmq.Again as e:
                pass
        print "Stopping Order Executor Server"

    def placeorder(self,action,symbol,ltp):
        if action=='BUY':
            price = ltp + (self.gui.offset * 0.5)
        else:
            action='SELL'
            price = ltp - (self.gui.offset * 0.5)
            # place kite order
        oid = self.kite.order_place(exchange="NSE", tradingsymbol=symbol, transaction_type=action,
                                    quantity=self.gui.quantity, price=price,
                                    product='MIS', order_type='LIMIT', validity='DAY', disclosed_quantity=None,
                                    trigger_price=None, squareoff_value=self.gui.profit,
                                    stoploss_value=self.gui.stopLoss, trailing_stoploss=None,
                                    variety='bo', tag='ByYogesh')
        self.db.addOrder(oid, symbol, action, price)