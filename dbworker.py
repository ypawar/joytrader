import sqlite3
from datetime import datetime
import logging


class DB:
    def __init__(self):
        self.logger = logging.getLogger('joytrader.'+__name__)
        self.connectDB()

    def connectDB(self):
        # print 'Connecting to DB'
        self.logger.info('Connecting to DB')
        self.db = sqlite3.connect('joytrading.sqlite', detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES,
                                  check_same_thread=False)
        self.db.execute(
            '''CREATE TABLE  IF NOT EXISTS "ltp" ("id" INTEGER PRIMARY KEY  NOT NULL ,"itoken" INTEGER NOT NULL ,"iltp" FLOAT NOT NULL , "time" timestamp)''')
        self.db.execute(
            '''CREATE TABLE IF NOT EXISTS "orders" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "orderid" INTEGER NOT NULL , "symbol" VARCHAR NOT NULL , "action" VARCHAR NOT NULL , "price" REAL, "time" timestamp)''')
        # print 'DB Connected joytrading.sqlite'
        self.logger.info('DB Connected joytrading.sqlite')

    def addLTP(self, token, ltp):
        self.db.execute('insert into ltp (itoken,iltp,time) values(?,?,?)', (token, ltp, datetime.now()))
        # self.db.commit()
        # print 'Added LTP for token', token
        self.logger.info('Added LTP for token {}'.format(token))

    def getLTP(self, token):
        out = self.db.execute('SELECT iltp FROM ltp  where itoken=? order by id desc limit 1', (token,))
        # print 'Fetched data for', token
        self.logger.info('Fetched data for {}'.format(token))
        return out.fetchone()[0]

    def addOrder(self, oid, sym, actn, price):
        self.db.execute('insert into orders (orderid,symbol,action,price,time) values(?,?,?,?,?)',
                        (oid, sym, actn, price, datetime.now()))
        # self.db.commit()
        # print 'Added', actn, 'Order for', sym
        self.logger.info('Added {} order for {}'.format(actn,sym))

    def getOrder(self):
        out = self.db.execute('select * from orders')
        return out.fetchall()

    def logout(self):
        self.db.commit()
        print 'Commited all DB of this transaction.'
        self.logger.info('Committed all DB of this transaction.')

