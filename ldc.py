from kiteconnect import WebSocket
import ninjatrader
from watchdog import Watchdog
import logging
import time

class LiveDataCollector:
    def __init__(self,apiscrt,ptoken,uid,dbctr,tknsymdict):
        self.logger = logging.getLogger('joytrader.'+__name__)
        # DB Connector
        self.db = dbctr
        # Token to Symbol Dictionary
        self.tknsym = tknsymdict
        # Token List
        self.tknlst = self.tknsym.keys()
        self.apiscrt = apiscrt
        self.ptoken = ptoken
        self.uid = uid
        # Get Ninja Trader Interface
        self.nt = ninjatrader.NT()
        # define watchdog whioch will reset the websocket if no data for more than 10 seconds
        self.watchdog = Watchdog(1, self.reconnect)
        # Start KiteConnect connection and keep running
        self.logger.info('Starting Websocket!')
        self.start()
        self.logger.info('Started Websocket!')
        # Wait 10 seconds before starting the watchdog
        # time.sleep(10)
        # print 'Starting Watchdog!'
        # self.watchdog.start()
        # print 'Watchdog Started'
        # self.logger.info('Watchdog Started')

    def start(self):
        # Kite Connect Websocket
        self.kws = WebSocket(self.apiscrt, self.ptoken, self.uid)
        # Assign the callbacks.
        self.kws.on_tick = self.ontick
        self.kws.on_connect = self.onconnect
        self.kws.connect(threaded=True)

    def ontick(self,tick,ws):
        # On every data stream save it to the DB
        for t in tick:
            # Add data to DB for later use
            token,price = t['instrument_token'],t['last_price']
            self.db.addLTP(token,price)
            # Send data to ninjatradre using NtDirect.dll api
            self.nt.SetLastPrice(self.tknsym[token],price,100)
            # print 'Sent ltp for',self.tknsym[token],'with',price,'to NT'
            self.logger.info('Sent ltp for {} with {} to NT'.format(self.tknsym[token],price))
        # Reset watchdog
        self.watchdog.reset()
        print self.kws.is_connected()

    def onconnect(self,ws):
        print "Connected Websocket!"
        self.logger.info("Connected Websocket!")
        # Subscribe token list
        ws.subscribe(self.tknlst)
        # Set LTP mode for all tokens
        ws.set_mode(ws.MODE_LTP,self.tknlst)
        print 'Subscribed for Token List'
        self.logger.info('Subscribed for Token List')

    def reconnect(self):
        print "Reconnecting Socket"
        self.kws.close()
        # time.sleep(0.1)
        self.logger.warning("Reconnecting Socket")
        # self.kws.reconnect()
        self.start()
        print "Reconnected Socket"
        self.logger.warning("Reconnected Socket")

    def close(self):
        # Close connection
        self.kws.close()
        print 'Closed Kite Websocket.'
        # print "Stopping Watchdog"
        self.logger.info('Closed Kite Websocket!')
        self.watchdog.stop()
        print "Stopped Watchdog"
        self.logger.info("Stopped Watchdog")
