# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'manual.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import dbworker
import joytrader
from kiteconnect import KiteConnect
import ldc
from os import path, remove
import logging
import time

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Form(object):
    def setupUi(self, Form,db,symtkndict):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(275, 178)
        self.verticalLayoutWidget = QtGui.QWidget(Form)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(0, -10, 201, 161))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.lineEditProfit = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.lineEditProfit.setObjectName(_fromUtf8("lineEditProfit"))
        self.gridLayout.addWidget(self.lineEditProfit, 3, 1, 1, 1)
        self.label_3 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout.addWidget(self.label_3, 3, 0, 1, 1)
        self.lineEditQuantity = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.lineEditQuantity.setObjectName(_fromUtf8("lineEditQuantity"))
        self.gridLayout.addWidget(self.lineEditQuantity, 1, 1, 1, 1)
        self.label = QtGui.QLabel(self.verticalLayoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.lineEditStopLoss = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.lineEditStopLoss.setObjectName(_fromUtf8("lineEditStopLoss"))
        self.gridLayout.addWidget(self.lineEditStopLoss, 4, 1, 1, 1)
        self.label_4 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.gridLayout.addWidget(self.label_4, 4, 0, 1, 1)
        self.lineEditOffset = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.lineEditOffset.setObjectName(_fromUtf8("lineEditOffset"))
        self.gridLayout.addWidget(self.lineEditOffset, 2, 1, 1, 1)
        self.label_2 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)
        self.comboBoxInstrument = QtGui.QComboBox(self.verticalLayoutWidget)
        self.comboBoxInstrument.setEditable(True)
        self.comboBoxInstrument.setObjectName(_fromUtf8("comboBoxInstrument"))
        self.gridLayout.addWidget(self.comboBoxInstrument, 0, 1, 1, 1)
        self.labelInstrument = QtGui.QLabel(self.verticalLayoutWidget)
        self.labelInstrument.setObjectName(_fromUtf8("labelInstrument"))
        self.gridLayout.addWidget(self.labelInstrument, 0, 0, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.verticalLayoutWidget_2 = QtGui.QWidget(Form)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(200, 0, 77, 141))
        self.verticalLayoutWidget_2.setObjectName(_fromUtf8("verticalLayoutWidget_2"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.pushButtonBuy = QtGui.QPushButton(self.verticalLayoutWidget_2)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButtonBuy.sizePolicy().hasHeightForWidth())
        self.pushButtonBuy.setSizePolicy(sizePolicy)
        self.pushButtonBuy.setObjectName(_fromUtf8("pushButtonBuy"))
        self.verticalLayout_2.addWidget(self.pushButtonBuy)
        self.pushButtonSell = QtGui.QPushButton(self.verticalLayoutWidget_2)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButtonSell.sizePolicy().hasHeightForWidth())
        self.pushButtonSell.setSizePolicy(sizePolicy)
        self.pushButtonSell.setObjectName(_fromUtf8("pushButtonSell"))
        self.verticalLayout_2.addWidget(self.pushButtonSell)
        self.pushButtonUpdate = QtGui.QPushButton(Form)
        self.pushButtonUpdate.setGeometry(QtCore.QRect(5, 150, 265, 23))
        self.pushButtonUpdate.setObjectName(_fromUtf8("pushButtonUpdate"))

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)
        self.db =db
        self.symtkn = symtkndict

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "JOY TRADER MANUAL", None))
        self.lineEditProfit.setText(_translate("Form", "5", None))
        self.label_3.setText(_translate("Form", "PROFIT", None))
        self.lineEditQuantity.setText(_translate("Form", "10", None))
        self.label.setText(_translate("Form", "QUANTITY", None))
        self.lineEditStopLoss.setText(_translate("Form", "5", None))
        self.label_4.setText(_translate("Form", "STOPLOSS", None))
        self.lineEditOffset.setText(_translate("Form", "2", None))
        self.label_2.setText(_translate("Form", "OFFSET", None))
        self.labelInstrument.setText(_translate("Form", "INSTRUMENT", None))
        self.pushButtonBuy.setText(_translate("Form", "BUY", None))
        self.pushButtonSell.setText(_translate("Form", "SELL", None))
        self.pushButtonUpdate.setText(_translate("Form", "UPDATE", None))
        self.intValidator = QtGui.QIntValidator(0, 9999)
        self.intValidator2 = QtGui.QIntValidator(0, 10)
        self.doubleValidator = QtGui.QDoubleValidator()

        self.lineEditOffset.setValidator(self.intValidator2)
        self.lineEditQuantity.setValidator(self.intValidator)
        self.lineEditProfit.setValidator(self.doubleValidator)
        self.lineEditStopLoss.setValidator(self.doubleValidator)

        self.instrument = ''
        self.orderType = ''
        self.orderVariety = ''
        self.product = ''
        self.quantity = 0
        self.quantity = int(self.lineEditQuantity.text())
        self.profit = float(self.lineEditProfit.text())
        self.stopLoss = float(self.lineEditStopLoss.text())
        self.offset = int(self.lineEditOffset.text())
        self.price = 100
        with open('symtknlist.txt') as f:
            lines = f.readlines()
        for i in lines:
            self.comboBoxInstrument.addItem(_fromUtf8(i.strip().split(',')[0]))
        self.pushButtonUpdate.clicked.connect(self.updateValues)

        self.pushButtonBuy.clicked.connect(self.buyClicked)
        self.pushButtonSell.clicked.connect(self.sellClicked)

    def updateValues(self):
        self.quantity = int(self.lineEditQuantity.text())
        self.profit = float(self.lineEditProfit.text())
        self.stopLoss = float(self.lineEditStopLoss.text())
        self.offset = int(self.lineEditOffset.text())
        self.instrument = str(self.comboBoxInstrument.currentText())
        self.price = self.db.getLTP(self.symtkn[self.instrument])
        # print "Values Updated", 'Quantity', self.quantity, 'Profit', self.profit, 'StopLoss', self.stopLoss, 'Offset', self.offset

    def buyClicked(self):
        self.updateValues()
        self.placeorder('BUY',self.instrument,self.price)
        print "Bought", self.instrument,"at", self.price + (self.offset * 0.05)


    def sellClicked(self):
        self.updateValues()
        self.placeorder('SELL', self.instrument, self.price)
        print "Sold", self.instrument,"at", self.price - (self.offset * 0.05)


    def placeorder(self,action,symbol,ltp):
        if action=='BUY':
            price = ltp + (self.offset * 0.05)
        else:
            action='SELL'
            price = ltp - (self.offset * 0.05)
            # place kite order
        oid = kite.order_place(exchange="NSE", tradingsymbol=symbol, transaction_type=action,
                                    quantity=self.quantity, price=price,
                                    product='MIS', order_type='LIMIT', validity='DAY', disclosed_quantity=None,
                                    trigger_price=None, squareoff_value=self.profit,
                                    stoploss_value=self.stopLoss, trailing_stoploss=None,
                                    variety='bo', tag='ByYogesh')
        self.db.addOrder(oid, symbol, action, price)

if __name__ == "__main__":
    # Remove existing log file if present
    if path.isfile("joytrader.log"):
        remove("joytrader.log")

    # Create the Logger
    logger = logging.getLogger('joytrader')
    logger.propagate = False
    logger.setLevel(logging.INFO)

    # Create the Handler for logging data to a file
    logger_handler = logging.FileHandler('joytrader.log')
    logger_handler.setLevel(logging.DEBUG)

    # Create a Formatter for formatting the log messages
    logger_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # Add the Formatter to the Handler
    logger_handler.setFormatter(logger_formatter)

    # Add the Handler to the Logger
    logger.addHandler(logger_handler)
    logger.info('Completed configuring logger()!')

    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    Form.setFixedSize(275, 178)
    ui = Ui_Form()
    db = dbworker.DB()
    logger.info('Creating Token Dictionaries')
    joytrader.makedicts()
    # # Setup Kite API
    kite = KiteConnect(api_key=joytrader.apikey)
    print kite.login_url()
    request_token = raw_input("Enter Request Token\n")
    data = kite.request_access_token(request_token, secret=joytrader.apisecret)
    kite.set_access_token(data["access_token"])

    while not joytrader.processExists("NinjaTrader.exe"):
        print "Please start Ninja Trader and then press any key"
        raw_input()
    # Setup live data collector and feeder for NT
    liveDataCollector= ldc.LiveDataCollector(joytrader.apisecret,data['public_token'],joytrader.userid,db,
                                             joytrader.tknsym)
    ui.setupUi(Form,db,joytrader.symtkn)
    Form.show()

    exits = app.exec_()
    liveDataCollector.close()
    db.logout()
    # Stop all other processes then exit
    time.sleep(2)
    sys.exit(exits)

